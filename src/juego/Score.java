package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Score {
	private int puntaje;
	private int ninjasEliminados;
	private int ramosEntregados;
	private boolean ramoEnMano;
	
	private Image imgPuntaje;
	private Image imgNinjaKills;
	private Image imgRamos;
	private Image imgBorde;
	private Image imgRamoEnMano;
	
	public Score() {
		puntaje = -5;
		ninjasEliminados = 0;
		ramosEntregados = -1;
		imgPuntaje = Herramientas.cargarImagen("score.png");
		imgNinjaKills = Herramientas.cargarImagen("ninjaKills.png");
		imgRamos = Herramientas.cargarImagen("ramosEntregados.png");
		imgBorde = Herramientas.cargarImagen("border.png");
	}

	public void dibujar(Entorno e) {
		imgRamoEnMano = (ramoEnMano ? Herramientas.cargarImagen("flower.png") : Herramientas.cargarImagen("flowerNot.png"));
		e.dibujarImagen(imgBorde, e.ancho() / 2, e.alto() / 2 - 1, 0);
		e.dibujarImagen(imgRamoEnMano, 42, 42, 0, 0.55);
		e.dibujarImagen(imgNinjaKills, e.ancho() - 35, e.alto() - 105, 0, 0.6);
		e.dibujarImagen(imgRamos, e.ancho() - 35, e.alto() - 65, 0, 0.6);
		e.dibujarImagen(imgPuntaje, e.ancho() - 50, e.alto() - 25, 0, 0.8);
		e.cambiarFont("Serif", 14, Color.RED);
		e.escribirTexto("" + ninjasEliminados, e.ancho() - 35, e.alto() - 100);
		e.cambiarFont("", 14, Color.white);
		e.escribirTexto("" + ramosEntregados, e.ancho() - 35, e.alto() - 58);
		e.cambiarFont("", 16, Color.yellow);
		e.escribirTexto("" + puntaje, e.ancho() - 35, e.alto() - 20);

	}

	public void sumarPuntos() {
		puntaje += 5;
	}
	
	public void sumarPuntosPorNinjasEliminados() {
		puntaje += 2;
	}
	
	public void sumarPuntosPorNinjaCasteadorEliminado() {
		puntaje += 5;
	}

	public void sumarNinjaEliminados() {
		ninjasEliminados += 1;
	}

	public void sumarRamoEntregado() {
		ramosEntregados += 1;
	}
	
	public void reiniciar() {
		puntaje = 0;
		ninjasEliminados = 0;
		ramosEntregados = 0;
		ramoEnMano = false;
	}
	
	public void ramoEnMano() {
		ramoEnMano = true;
	}
	
	public void sinRamoEnMano() {
		ramoEnMano = false;
	}
	public int getPuntaje() {
		return puntaje;
	}
}
