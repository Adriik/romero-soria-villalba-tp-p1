package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;

	private Sakura sakura;
	private Rasengan rasengan;

	private Manzana[][] manzanas;
	private Calle[][] calles;

	private Casa[] casas;
	private Casa casaDelPedido;
	private Casa floreria;
	private Flecha flecha;

	private Ninja[] ninjas;
	private Ninja[] ninjasRebotadores;
	private Kunai[] kunais;
	private Ninja ninjaCasteador;
	private Jutsu jutsu;

	private Score score;
	private int contadorTickLanzarKunai;
	private int contadorTickNinjaCasteador;
	private int contadorTickRespawnear;

	private Clip musica;
	private boolean empiezaLaMusica;

	private Image imgFondo;
	private Image imgGameOver;

	public Juego() {
		entorno = new Entorno(this, "Sakura Ikebana Delivery - Grupo 5", 800, 600);
		int tablero = 15;
		manzanas = new Manzana[tablero][tablero];
		calles = new Calle[manzanas.length][manzanas.length];

		double divisionEntornoHorizontal = entorno.ancho() / manzanas.length;
		double divisionEntornoVertical = entorno.alto() / manzanas.length;

		int countVertical = 0;
		for (int i = 0; i < manzanas.length; i++) {
			int countHorizontal = 0;
			for (int j = 0; j < manzanas[i].length; j++) {

				if ((countHorizontal < 3 && countHorizontal >= 0 && countVertical < 3 && countVertical >= 0)) {
					manzanas[i][j] = new Manzana(
							(((j + 1) * (divisionEntornoHorizontal)) - (divisionEntornoHorizontal)),
							(((i + 1) * (divisionEntornoVertical)) - (entorno.alto() / manzanas[i].length)),
							divisionEntornoHorizontal, entorno.alto() / manzanas[i].length);
				} else {
					calles[i][j] = new Calle((((j + 1) * (divisionEntornoHorizontal)) - (divisionEntornoHorizontal)),
							(((i + 1) * (divisionEntornoVertical)) - (entorno.alto() / manzanas[i].length)),
							divisionEntornoHorizontal, entorno.alto() / manzanas[i].length, i, j);
				}
				countHorizontal++;
				if (countHorizontal >= 3) {
					countHorizontal = -1;
				}
			}
			countVertical++;
			if (countVertical >= 3) {
				countVertical = -1;
			}
		}

		// Generar las casas
		casas = new Casa[32];
		int cont = 0;
		for (int i = 0; i < manzanas.length; i += 4) {
			for (int j = 0; j < manzanas[i].length; j += 4) {
				casas[cont] = new Casa(manzanas[i][j].getX() + manzanas[i][j].getAncho() / 1.5,
						manzanas[i][j].getY() + manzanas[i][j].getAlto() / 1.5,
						manzanas[i][j].getAncho() + manzanas[i][j].getAncho() / 4,
						manzanas[i][j].getAlto() + manzanas[i][j].getAlto() / 4, false);
				cont++;
			}
		}
		for (int i = 2; i < manzanas.length; i += 4) {
			for (int j = 2; j < manzanas[i].length; j += 4) {
				casas[cont] = new Casa(manzanas[i][j].getX() + manzanas[i][j].getAncho() / 3,
						manzanas[i][j].getY() + manzanas[i][j].getAlto() / 3,
						manzanas[i][j].getAncho() + manzanas[i][j].getAncho() / 4,
						manzanas[i][j].getAlto() + manzanas[i][j].getAlto() / 4,
						cont == casas.length - 2 ? true : false);
				cont++;
			}
		}

		sakura = new Sakura(entorno);

		// Generar ninjas
		ninjas = new Ninja[3];
		int filaCalle = 3;
		int columnaCalle = 3;
		int direccion = 0;

		for (int i = 0; i < ninjas.length; i++) {
			direccion = ((i + 1) % 2) + 1;
			ninjas[i] = new Ninja(
					calles[filaCalle][columnaCalle].getX() + calles[filaCalle][columnaCalle].getAncho() / 2,
					calles[filaCalle][columnaCalle].getY() + calles[filaCalle][columnaCalle].getAlto() / 2, 0, 2,
					direccion);
			filaCalle += 4;
			columnaCalle += 4;
		}

		ninjasRebotadores = null;
		kunais = null;
		ninjaCasteador = null;

		flecha = null;
		casaDelPedido = null;
		floreria = casas[casas.length - 2];

		score = new Score();
		contadorTickLanzarKunai = 0;
		contadorTickNinjaCasteador = 0;
		contadorTickRespawnear = 0;

		imgFondo = Herramientas.cargarImagen("konoha.png");
		imgGameOver = Herramientas.cargarImagen("gameOver.png");
		musica = Herramientas.cargarSonido("music.wav");
		entorno.iniciar();
	}

	// ###################################### //
	public void tick() {
		if (!empiezaLaMusica) {
			musica.loop(-1);
			empiezaLaMusica = true;
		}

		if (sakura == null) {
			entorno.dibujarImagen(imgGameOver, entorno.ancho() / 2, entorno.alto() / 2, 0);
			score.dibujar(entorno);
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				score.reiniciar();
				Ninja.respawn(ninjas, calles);
				ninjasRebotadores = null;
				kunais = null;
				ninjaCasteador = null;
				jutsu = null;
				rasengan = null;
				sakura = new Sakura(entorno);
			}
			return;
		}

		entorno.dibujarImagen(imgFondo, entorno.ancho() / 2, entorno.alto() / 2, 0);

		contadorTickRespawnear++;

		// Elegir la casa del pedido
		if (casaDelPedido == null || (sakura.estasEnLaZonaDeEntrega(casaDelPedido.zonaDeEntregaHorizontal(entorno),
				casaDelPedido.zonaDeEntregaVertical(entorno))) && sakura.tieneElRamo()) {
			sakura.dejaElRamo();
			flecha = new Flecha(casas);
			casaDelPedido = flecha.getCasaElegida();
			score.sumarPuntos();
			score.sumarRamoEntregado();
			score.sinRamoEnMano();
		}

		if (sakura.estasEnLaZonaDeEntrega(floreria.zonaDeEntregaHorizontal(entorno),
				floreria.zonaDeEntregaVertical(entorno)) && !sakura.tieneElRamo()) {
			sakura.recibeElRamo(entorno);
			score.ramoEnMano();
		}
		
		sakura.dibujar(entorno);

		// ####### MOVIMIENTOS SAKURA ####### //

		Calle estaEnEstaCalle = sakura.enQueCalleEsta(calles);

		if (entorno.estaPresionada('a') && !sakura.estasPorSalirDelEntornoPorIzquierda()
				&& sakura.sePuedeMoverHorizontal(calles, estaEnEstaCalle)) {
			sakura.moverIzquierda();
		} else if (entorno.estaPresionada('d') && !sakura.estasPorSalirDelEntornoPorDerecha(entorno)
				&& sakura.sePuedeMoverHorizontal(calles, estaEnEstaCalle)) {
			sakura.moverDerecha();
		} else if (entorno.estaPresionada('w') && !sakura.estasPorSalirDelEntornoPorArriba()
				&& sakura.sePuedeMoverVertical(calles, estaEnEstaCalle)) {
			sakura.moverArriba();
		} else if (entorno.estaPresionada('s') && !sakura.estasPorSalirDelEntornoPorAbajo(entorno)
				&& sakura.sePuedeMoverVertical(calles, estaEnEstaCalle)) {
			sakura.moverAbajo();
		} else {
			sakura.movimientoOFF();
		}

		if (entorno.sePresiono(entorno.TECLA_ESPACIO) && rasengan == null) {
			rasengan = sakura.lanzarRasengan();
		}
		if (rasengan != null) {
			if (!rasengan.debeDesaparecer()) {
				rasengan.mover();
				rasengan.dibujar(entorno);
			} else {
				rasengan = null;
				sakura.podesLanzarUnRasengan();
			}
		}
		// ####### ------------------ ####### //

		// ninjas (nivel 1)
		for (Ninja v : ninjas) {
			if (v != null) {
				v.dibujar(entorno);
			}
		}

		for (int i = 0; i < ninjas.length; i++) {
			if (ninjas[i] == null) {
				continue;
			}
			ninjas[i].mover(entorno, manzanas.length);
		}

		// ninjas rebotadores (nivel 2)
		if (score.getPuntaje() >= 10 && ninjasRebotadores == null) {
			ninjasRebotadores = new Ninja[2];
			kunais = new Kunai[ninjasRebotadores.length];

			int filaCalle = 3;
			int columnaCalle = 3;

			for (int i = 0; i < ninjasRebotadores.length; i++) {
				ninjasRebotadores[i] = new Ninja(
						calles[columnaCalle][filaCalle].getX() + calles[columnaCalle][columnaCalle].getAncho() / 2,
						calles[columnaCalle][filaCalle].getY() + calles[filaCalle][filaCalle].getAlto() / 2, 1, 3, 1);
				columnaCalle += 8;
				filaCalle += 8;
			}
		}

		if (ninjasRebotadores != null) {
			contadorTickLanzarKunai++;
			for (Ninja v : ninjasRebotadores) {
				if (v != null) {
					v.dibujar(entorno);
				}
			}

			for (int i = 0; i < ninjasRebotadores.length; i++) {
				if (ninjasRebotadores[i] == null) {
					continue;
				}
				ninjasRebotadores[i].mover(entorno, manzanas.length);
			}

			// Lanzamiento de kunai
			if (contadorTickLanzarKunai >= 400) {
				for (int i = 0; i < ninjasRebotadores.length; i++) {
					if (ninjasRebotadores[i] != null && kunais[i] == null && !ninjasRebotadores[i].tieneQueDesaparecer()
							&& !ninjasRebotadores[i].tieneQueAparecer()) {
						kunais[i] = ninjasRebotadores[i].lanzarKunai();
					}
				}
				contadorTickLanzarKunai = 0;
			}

			for (int i = 0; i < ninjasRebotadores.length; i++) {
				if (kunais[i] != null) {
					if (!kunais[i].seSalioDelEntorno(entorno, manzanas.length)) {
						kunais[i].dibujar(entorno);
						kunais[i].mover();
					} else {
						kunais[i] = null;
					}
				}
			}
		}

		// ninja casteador (nivel 3)
		if (score.getPuntaje() >= 15) {
			contadorTickNinjaCasteador++;

			int filaCalleRandom = (int) (Math.random() * 10) == 0 ? 1 : (int) (Math.random() * 10);

			// Elegir calle horizontal aleatoria
			if (filaCalleRandom <= 3) { // Valores = {1, 2, 3}
				filaCalleRandom = 3;
			} else if (filaCalleRandom <= 6) { // Valores = {4, 5, 6}
				filaCalleRandom = 7;
			} else { // Valores = {7, 8, 9}
				filaCalleRandom = 11;
			}

			if (contadorTickNinjaCasteador == 500 && ninjaCasteador == null) {
				ninjaCasteador = new Ninja(
						calles[filaCalleRandom][0].getX() + calles[filaCalleRandom][0].getAncho() / 2,
						calles[filaCalleRandom][0].getY() + calles[filaCalleRandom][0].getAlto() / 2, 2, 2, 2);
				contadorTickNinjaCasteador = 0;
			}

			if (ninjaCasteador != null) {
				ninjaCasteador.dibujar(entorno);
			}

			if (contadorTickNinjaCasteador == 210) {
				ninjaCasteador = null;
			}

			// Ataque con jutsu de ninjaCasteador
			if (contadorTickNinjaCasteador == 70) {
				if (ninjaCasteador != null && !ninjaCasteador.completasteLaSubstitucion()) {
					jutsu = ninjaCasteador.tirarJutsu();
				}
			}

			// Jutsu
			if (jutsu != null && !jutsu.seSalioDelEntorno(entorno, manzanas.length)) {
				jutsu.dibujar(entorno);
				jutsu.mover();
			} else {
				jutsu = null;
			}
		}

		// ####### -------- Eliminar a los ninjas ---------- ####### //

		for (int i = 0; i < ninjas.length; i++) {
			if (rasengan != null && ninjas[i] != null && rasengan.chocaANinja(ninjas[i])
					&& !ninjas[i].completasteLaSubstitucion() && !ninjas[i].tieneQueAparecer()) {
				rasengan = null;
				ninjas[i].substitucion();
				sakura.podesLanzarUnRasengan();
			}
			if (ninjas[i] != null && ninjas[i].completasteLaSubstitucion()) {
				ninjas[i] = null;
				score.sumarNinjaEliminados();
				score.sumarPuntosPorNinjasEliminados();
			}
		}
		
		if (ninjasRebotadores != null) {
			for (int i = 0; i < ninjasRebotadores.length; i++) {
				if (rasengan != null && ninjasRebotadores[i] != null && rasengan.chocaANinja(ninjasRebotadores[i])
						&& !ninjasRebotadores[i].tieneQueAparecer()) {
					rasengan = null;
					ninjasRebotadores[i].substitucion();
					sakura.podesLanzarUnRasengan();
				}
				if (ninjasRebotadores[i] != null && ninjasRebotadores[i].completasteLaSubstitucion()) {
					ninjasRebotadores[i] = null;
					score.sumarNinjaEliminados();
					score.sumarPuntosPorNinjasEliminados();
				}
			}
		}
		if (rasengan != null && ninjaCasteador != null && rasengan.chocaANinja(ninjaCasteador)) {
			ninjaCasteador.substitucion();
			rasengan = null;
			sakura.podesLanzarUnRasengan();

			if (ninjaCasteador.completasteLaSubstitucion()) {
				ninjaCasteador = null;
				score.sumarNinjaEliminados();
				score.sumarPuntosPorNinjaCasteadorEliminado();
			}
		}

		// ####### -------- Regenerar a los ninjas ---------- ####### //

		if ((contadorTickRespawnear / 100) % 15 == 0) {
			Ninja.respawn(ninjas, calles);
			if (ninjasRebotadores != null) {
				Ninja.respawnRebotadores(ninjasRebotadores, calles);
			}
		}

		// ####### --------Perder el juego---------- ####### //

		for (int i = 0; i < ninjas.length; i++) {
			if (ninjas[i] != null && sakura.chocaConNinja(ninjas[i])) {
				sakura = null;
				break;
			}
		}
		if (ninjasRebotadores != null) {
			for (int i = 0; i < ninjasRebotadores.length; i++) {
				if (sakura != null && ((ninjasRebotadores[i] != null && sakura.chocaConNinja(ninjasRebotadores[i]))
						|| (kunais[i] != null && sakura.chocaConKunai(kunais[i])))) {
					sakura = null;
					break;
				}
			}
		}
		if (sakura != null && ((ninjaCasteador != null && sakura.chocaConNinja(ninjaCasteador))
				|| (jutsu != null && sakura.chocaConJutsu(jutsu)))) {
			sakura = null;
		}

		flecha.dibujar(entorno);
		score.dibujar(entorno);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
