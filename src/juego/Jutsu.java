package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Jutsu {
	private double x;
	private double y;

	private int velocidad;

	private Image img;
	private int tipoDeJutsu;

	public Jutsu(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 5;
		this.tipoDeJutsu = (int) (Math.random() * 10 + 1);

	}

	public void dibujar(Entorno entorno) {
		if (tipoDeJutsu <= 2) {
			img = Herramientas.cargarImagen("thunder-attack.gif");
		} else if (tipoDeJutsu <= 4) {
			img = Herramientas.cargarImagen("fire-attack.gif");
		} else if (tipoDeJutsu <= 6) {
			img = Herramientas.cargarImagen("meteo-attack.gif");
		} else if (tipoDeJutsu <= 8) {
			img = Herramientas.cargarImagen("acqua-attack.gif");
		} else {
			img = Herramientas.cargarImagen("aereo-attack.gif");
		}

		entorno.dibujarImagen(img, x, y, 0, 1);
	}

	public void mover() {
		x += velocidad;
	}

	public boolean seSalioDelEntorno(Entorno entorno, double offset) {
		double offsetHorizontal = entorno.ancho() / offset;

		return x - offsetHorizontal > entorno.ancho();
	}

	// Getters
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

}
