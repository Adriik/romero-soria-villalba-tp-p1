package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Kunai {

	private double x;
	private double y;
	private int direccion;
	private int velocidad;
	private Image img;

	public Kunai(double x, double y, int direccion) {
		this.x = x;
		this.y = y;
		this.direccion = direccion;
		velocidad = 5;
	}

	public void dibujar(Entorno entorno) {
		img = Herramientas.cargarImagen(direccion == 1 || direccion == 2 ? "kunai-right.png" : "kunai-left.png");

		entorno.dibujarImagen(img, x, y, 0, 2);
	}

	public void mover() {
		if (direccion == 1) {
			x += velocidad;
		}
		if (direccion == -1) {
			x -= velocidad;
		}
	}

	public boolean seSalioDelEntorno(Entorno entorno, double offset) {
		double offsetHorizontal = entorno.ancho() / offset;

		return (x + offsetHorizontal < 0 || x - offsetHorizontal > entorno.ancho());
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
