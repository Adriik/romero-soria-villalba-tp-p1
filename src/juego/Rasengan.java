package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Rasengan {

	private double x;
	private double y;
	private int direccion;
	private int longitudDeMovimiento;
	private double velocidad;
	private Image img;
	private int tiempoDeCargaDelRasengan;

	public Rasengan(double x, double y, int direccion) {
		this.x = x - 2;
		this.y = y - 2;
		this.direccion = direccion;
		longitudDeMovimiento = 45;
		velocidad = 5;
		tiempoDeCargaDelRasengan = 0;
	}

	public void dibujar(Entorno e) {
		if (tiempoDeCargaDelRasengan < 25) {
			return;
		}
		img = Herramientas.cargarImagen("rasengan.gif");
		e.dibujarImagen(img, x, y, 0, 0.11);
	}

	public void mover() {
		if (tiempoDeCargaDelRasengan < 25) {
			tiempoDeCargaDelRasengan++;
			return;
		}
		longitudDeMovimiento -= 1;
		if (direccion == 1) {
			x -= velocidad;
		} else if (direccion == 2) {
			x += velocidad;
		} else if (direccion == 3) {
			y -= velocidad;
		} else if (direccion == 4) {
			y += velocidad;
		}
	}

	public boolean debeDesaparecer() {
		return (x > x + longitudDeMovimiento) || (x < x - longitudDeMovimiento) || (y > y + longitudDeMovimiento)
				|| (y < y - longitudDeMovimiento);
	}

	public boolean chocaANinja(Ninja ninja) {
		return x <= ninja.getX() + 30 && x + 30 >= ninja.getX() && y <= ninja.getY() + 30 && y + 30 >= ninja.getY();
	}
}
