package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Flecha {

	private double x;
	private double y;

	private Casa casaElegida;

	private Image img;

	public Flecha(Casa[] casas) {
		casaElegida = elegirAlgunaCasa(casas);
		x = casaElegida.getX();
		y = casaElegida.getY();
		img = Herramientas.cargarImagen("arrow-vertical.gif");
	}

	public void dibujar(Entorno e) {
		if (casaElegida.getY() < 50) {
			img = Herramientas.cargarImagen("arrow-horizontal.gif");
			e.dibujarImagen(img, x - 40, y, 0, 0.8);
			return;
		}
		e.dibujarImagen(img, x - 10, y - 35, 0, 0.8);
	}

	private Casa elegirAlgunaCasa(Casa[] c) {
		int randomCasa = 0;
		boolean elegiCasa = false;

		while (!elegiCasa) {
			randomCasa = (int) (Math.random() * (c.length - 1));
			if (!c[randomCasa].esFloreria() && randomCasa != 0) {
				elegiCasa = true;
			}
		}
		return c[randomCasa];
	}

	public Casa getCasaElegida() {
		return casaElegida;
	}

}
