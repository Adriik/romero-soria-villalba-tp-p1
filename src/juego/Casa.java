package juego;

import java.awt.Rectangle;
import entorno.Entorno;

public class Casa {

	private double x;
	private double y;

	private double ancho;
	private double alto;

	private boolean esFloreria;

	public Casa(double x, double y, double ancho, double alto, boolean esFloreria) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.esFloreria = esFloreria;
	}

	public Rectangle zonaDeEntregaHorizontal(Entorno e) {
		Rectangle rectangulo = new Rectangle((int) x, (int) y, (int) ancho * 3, (int) alto);
		return rectangulo;
	}

	public Rectangle zonaDeEntregaVertical(Entorno e) {
		Rectangle rectangulo = new Rectangle((int) x, (int) y, (int) ancho, (int) alto * 3);
		return rectangulo;
	}

	// ------------- getters --------------
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public boolean esFloreria() {
		return esFloreria;
	}

}
