package juego;

public class Calle {
	private double x;
	private double y;

	private double ancho;
	private double alto;

	private int calleHorizontal;
	private int calleVertical;

	public Calle(double x, double y, double ancho, double alto, int calleVertical, int calleHorizontal) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.calleVertical = calleVertical;
		this.calleHorizontal = calleHorizontal;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	public int getCalleHorizontal() {
		return calleHorizontal;
	}

	public int getCalleVertical() {
		return calleVertical;
	}
}
