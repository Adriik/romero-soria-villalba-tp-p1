package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Ninja {
	private double x;
	private double y;
	private double velocidad;
	private Image img;
	private int tiempoQueTardaEnCargarElJutsu;
	private boolean tengoQueDesaparecer;
	private boolean substitucionCompleta;
	private boolean tengoQueAparecer;
	private int tiempoQueTardaEnAparecerYDesaparecer;

	// Direccion = 1: este
	// Direccion = -1: oeste
	// Direccion = 2: norte
	// Direccion = -2: sur
	private int direccion;

	// Ninjas = 0: transpasan la pantalla, por lo que aparecen del lado contrario.
	// Ninjas = 1: rebotan al chocar contra la pantalla, cambiando su dirección.
	// Ninjas = 2: después de cierto tiempo, aparecen en algún costado de la calle,
	// lanzando un ataque que ocupa toda la fila de calle.
	private int tipoDeNinja;

	public Ninja(double x, double y, int tipoDeNinja, int velocidad, int direccion) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.direccion = direccion;
		this.tipoDeNinja = tipoDeNinja;
		tengoQueAparecer = true;
	}

	public void dibujar(Entorno entorno) {
		if (tipoDeNinja == 0) {
			img = Herramientas.cargarImagen(direccion == 1 || direccion == 2 ? "ninjas-sprite/ninja-run-right.gif"
					: "ninjas-sprite/ninja-run-left.gif");
		} else if (tipoDeNinja == 1) {
			img = Herramientas.cargarImagen(direccion == 1 || direccion == 2 ? "ninjas-sprite/ninja-sound-run-right.gif"
					: "ninjas-sprite/ninja-sound-run-left.gif");
		} else if (tipoDeNinja == 2) {
			// Animacion para ninja de tipo 2
			// Varios if debido a que se dividió en subanimaciones
			// Así siempre empieza desde el principio
			if (tiempoQueTardaEnCargarElJutsu <= 10) {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-show-right.gif");
			} else if (tiempoQueTardaEnCargarElJutsu <= 15) {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-betweenframes-right.png");
			} else if (tiempoQueTardaEnCargarElJutsu <= 70) {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-spelling-right.gif");
			} else if (tiempoQueTardaEnCargarElJutsu <= 200) {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-attack-right.gif");
			} else if (tiempoQueTardaEnCargarElJutsu <= 205) {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-betweenframes-right.png");
			} else {
				img = Herramientas.cargarImagen("ninjas-sprite/ninja-mistery-show-right.gif");
			}
		}

		if (tengoQueDesaparecer || tengoQueAparecer && tipoDeNinja != 2) {

			if (tiempoQueTardaEnAparecerYDesaparecer <= 30) {
				img = Herramientas.cargarImagen("ninjas-sprite/substitution-0.gif");
			} else if (tiempoQueTardaEnAparecerYDesaparecer <= 60 && !tengoQueAparecer) {
				img = Herramientas.cargarImagen("ninjas-sprite/substitution-1.gif");
			} else if (tiempoQueTardaEnAparecerYDesaparecer <= 90 && !tengoQueAparecer) {
				img = Herramientas.cargarImagen("ninjas-sprite/substitution-2.gif");
			} else if (tiempoQueTardaEnAparecerYDesaparecer <= 120) {
				img = Herramientas.cargarImagen("ninjas-sprite/substitution-3.gif");
			} else {
				tengoQueAparecer = false;
				tiempoQueTardaEnAparecerYDesaparecer = 0;
			}

			tiempoQueTardaEnAparecerYDesaparecer++;
			if (tiempoQueTardaEnAparecerYDesaparecer == 120 && !tengoQueAparecer) {
				substitucionCompleta = true;
			}
		}
		tiempoQueTardaEnCargarElJutsu++;

		entorno.dibujarImagen(img, x, y, 0, tipoDeNinja == 0 ? 1.1 : 1);
	}

	public void mover(Entorno entorno, double offset) {
		double offsetHorizontal = entorno.ancho() / offset;
		double offsetVertical = entorno.alto() / offset;

		if (tengoQueAparecer || tengoQueDesaparecer) {
			return;
		}

		// Ver si el ninja tipo 0 se salió de la pantalla
		if (tipoDeNinja == 0) {
			// Comprobar en x
			if (x + offsetHorizontal < 0) {
				x = entorno.ancho() + offsetHorizontal;
			} else if (x - offsetHorizontal > entorno.ancho()) {
				x = 0 - offsetHorizontal;
				// Comprobar en Y
			} else if (y + offsetVertical < 0) {
				y = entorno.alto() + offsetVertical;
			} else if (y - offsetVertical > entorno.alto()) {
				y = 0 - offsetVertical;
			}
		} else if (tipoDeNinja == 1) {
			// Rebote en X
			if (x + offsetHorizontal / 2 > entorno.ancho() || x - offsetHorizontal / 2 < 0) {
				direccion *= -1;
			}

			// Rebote en Y
			if (y + offsetVertical / 2 > entorno.alto() || y - offsetVertical / 2 < 0) {
				direccion *= -1;
			}
		}

		if (direccion == 1) { // Este
			x += velocidad;
		} else if (direccion == -1) { // Oeste
			x -= velocidad;
		} else if (direccion == 2) { // Norte
			y += velocidad;
		} else if (direccion == -2) { // Sur
			y -= velocidad;
		}
	}

	public Kunai lanzarKunai() {
		return new Kunai(x, y, direccion);
	}

	public Jutsu tirarJutsu() {
		return new Jutsu(x, y);
	}

	public static void respawn(Ninja[] ninjas, Calle[][] calles) {
		int direccion = 0;
		int filaCalle = 3;
		int columnaCalle = 3;

		for (int i = 0; i < ninjas.length; i++) {
			if (ninjas[i] == null) {
				direccion = ((i + 1) % 2) + 1;

				ninjas[i] = new Ninja(
						calles[filaCalle][columnaCalle].getX() + calles[filaCalle][columnaCalle].getAncho() / 2,
						calles[filaCalle][columnaCalle].getY() + calles[filaCalle][columnaCalle].getAlto() / 2, 0, 2,
						direccion);

			}
			filaCalle += 4;
			columnaCalle += 4;
		}
	}

	public static void respawnRebotadores(Ninja[] ninjasRebotadores, Calle[][] calles) {
		int filaCalle = 3;

		for (int i = 0; i < ninjasRebotadores.length; i++) {
			if (ninjasRebotadores[i] == null) {
				ninjasRebotadores[i] = new Ninja(calles[filaCalle][0].getX() + calles[filaCalle][0].getAncho(),
						calles[filaCalle][0].getY() + calles[filaCalle][0].getAlto() / 2, 1, 3, 1);
			}
			filaCalle += 8;
		}
	}

	public void substitucion() {
		tengoQueDesaparecer = true;
	}

	// ------------ Getters -----------
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public boolean tieneQueDesaparecer() {
		return tengoQueDesaparecer;
	}

	public boolean completasteLaSubstitucion() {
		return substitucionCompleta;
	}

	public boolean tieneQueAparecer() {
		return tengoQueAparecer;
	}
}
