package juego;

import java.awt.Image;
import java.awt.Rectangle;

import entorno.Entorno;
import entorno.Herramientas;

public class Sakura {
	private double x;
	private double y;
	private double velocidad;
	private Image img;
	private Ramo ramo; // Determina si Sakura tiene el ramo.
	private int aDondeEstaMirando; // 1 = izquierda, 2 = derecha, 3 = arriba, 4 = abajo.
	private boolean modoDeMovimiento; // false: se dibuja sakura-idle, true: se dibuja sakura-run

	private boolean lanceUnRasengan;
	private int tiempoDeCargaDelRasengan; // Determina que frame se dibuja

	public Sakura(Entorno e) {
		x = e.ancho() / 2;
		y = e.alto() / 3 - 5;
		velocidad = 2;
		lanceUnRasengan = false;
		tiempoDeCargaDelRasengan = 0;
		aDondeEstaMirando = 4;
		img = Herramientas.cargarImagen("sakura-sprite/sakura-idle-rigth.gif");
	}

	public void dibujar(Entorno e) {

		if (lanceUnRasengan && tiempoDeCargaDelRasengan < 50) { // Evita que se dibujan otras imágenes hasta que termine
																// esta

			if (aDondeEstaMirando == 2 || aDondeEstaMirando == 4) {
				if (tiempoDeCargaDelRasengan < 10) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-right-0.png");
				} else if (tiempoDeCargaDelRasengan < 20) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-right-1.png");
				} else if (tiempoDeCargaDelRasengan < 30) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-right-2.png");
				} else if (tiempoDeCargaDelRasengan < 40) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-right-3.png");
				} else if (tiempoDeCargaDelRasengan < 50) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-right-4.png");
				}

			} else {
				if (tiempoDeCargaDelRasengan < 10) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-left-0.png");
				} else if (tiempoDeCargaDelRasengan < 20) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-left-1.png");
				} else if (tiempoDeCargaDelRasengan < 30) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-left-2.png");
				} else if (tiempoDeCargaDelRasengan < 40) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-left-3.png");
				} else if (tiempoDeCargaDelRasengan < 50) {
					img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-left-4.png");
				}
			}

			if (ramo != null) {
				if (aDondeEstaMirando == 2 || aDondeEstaMirando == 4) {
					if (tiempoDeCargaDelRasengan < 10) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-right-0.png");
					} else if (tiempoDeCargaDelRasengan < 20) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-right-1.png");
					} else if (tiempoDeCargaDelRasengan < 30) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-right-2.png");
					} else if (tiempoDeCargaDelRasengan < 40) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-right-3.png");
					} else if (tiempoDeCargaDelRasengan < 50) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-right-4.png");
					}

				} else {
					if (tiempoDeCargaDelRasengan < 10) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-left-0.png");
					} else if (tiempoDeCargaDelRasengan < 20) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-left-1.png");
					} else if (tiempoDeCargaDelRasengan < 30) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-left-2.png");
					} else if (tiempoDeCargaDelRasengan < 40) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-left-3.png");
					} else if (tiempoDeCargaDelRasengan < 50) {
						img = Herramientas.cargarImagen("sakura-sprite/sakura-rasengan-flor-left-4.png");
					}
				}
			}
			e.dibujarImagen(img, x, y - 5, 0, 0.85);
			tiempoDeCargaDelRasengan++;
			return;
		}

		if (aDondeEstaMirando == 3) {
			img = Herramientas.cargarImagen("sakura-sprite/sakura-run-left.gif");
		}

		if (aDondeEstaMirando == 4) {
			img = Herramientas.cargarImagen("sakura-sprite/sakura-run-right.gif");
		}

		if (aDondeEstaMirando == 1) {
			img = Herramientas.cargarImagen("sakura-sprite/sakura-run-left.gif");
		}

		if (aDondeEstaMirando == 2) {
			img = Herramientas.cargarImagen("sakura-sprite/sakura-run-right.gif");
		}

		if (!modoDeMovimiento) {
			img = Herramientas.cargarImagen(
					aDondeEstaMirando == 1 || aDondeEstaMirando == 3 ? "sakura-sprite/sakura-idle-left.gif"
							: "sakura-sprite/sakura-idle-rigth.gif");
		}

		if (ramo != null) {

			if (aDondeEstaMirando == 3) {
				img = Herramientas.cargarImagen("sakura-sprite/sakura-run-flor-left.gif");
			}

			if (aDondeEstaMirando == 4) {
				img = Herramientas.cargarImagen("sakura-sprite/sakura-run-flor-right.gif");
			}

			if (aDondeEstaMirando == 1) {
				img = Herramientas.cargarImagen("sakura-sprite/sakura-run-flor-left.gif");
			}

			if (aDondeEstaMirando == 2) {
				img = Herramientas.cargarImagen("sakura-sprite/sakura-run-flor-right.gif");
			}

			if (!modoDeMovimiento) {
				img = Herramientas.cargarImagen(
						aDondeEstaMirando == 1 || aDondeEstaMirando == 3 ? "sakura-sprite/sakura-flor-left.gif"
								: "sakura-sprite/sakura-flor-right.gif");
			}
		}
		e.dibujarImagen(img, x, y - 5, 0, 0.85);
	}

	public void moverArriba() {
		aDondeEstaMirando = 3;
		if (tiempoDeCargaDelRasengan == 0 || tiempoDeCargaDelRasengan > 40) {
			y -= velocidad;
			modoDeMovimiento = true;
		}
	}

	public void moverIzquierda() {
		aDondeEstaMirando = 1;
		if (tiempoDeCargaDelRasengan == 0 || tiempoDeCargaDelRasengan > 40) {
			x -= velocidad;
			modoDeMovimiento = true;
		}
	}

	public void moverAbajo() {
		aDondeEstaMirando = 4;
		if (tiempoDeCargaDelRasengan == 0 || tiempoDeCargaDelRasengan > 40) {
			y += velocidad;
			modoDeMovimiento = true;
		}
	}

	public void moverDerecha() {
		aDondeEstaMirando = 2;
		if (tiempoDeCargaDelRasengan == 0 || tiempoDeCargaDelRasengan > 40) {
			x += velocidad;
			modoDeMovimiento = true;
		}
	}

	// ####### ------------------------------- ####### //

	public boolean estasPorSalirDelEntornoPorIzquierda() {
		return x - 20 < 0;
	}

	public boolean estasPorSalirDelEntornoPorDerecha(Entorno e) {
		return x + 20 > e.ancho();
	}

	public boolean estasPorSalirDelEntornoPorArriba() {
		return y - 20 < 0;
	}

	public boolean estasPorSalirDelEntornoPorAbajo(Entorno e) {
		return y + 20 > e.alto();
	}

	// ####### ------------------------------- ####### //

	public Rasengan lanzarRasengan() {
		lanceUnRasengan = true; // Da comienzo a la animación de lanzar un rasengan y evita que pueda lanzar
								// otro.
		return new Rasengan(x, y, aDondeEstaMirando);
	}

	public void podesLanzarUnRasengan() {
		tiempoDeCargaDelRasengan = 0; // Resetea el tiempo de animación
		lanceUnRasengan = false; // Pone fin a la animación de lanzar un rasengan y permite lanzar otro.
	}
	
	// ####### ------------------------------- ####### //

	// ####### Verificar la posicion de Sakura ####### //
	public Calle enQueCalleEsta(Calle[][] calles) {
		for (int i = 0; i < calles.length; i++) {
			for (int j = 0; j < calles[i].length; j++) {
				if (calles[i][j] != null && estaDentroDeLaCalle(calles[i][j])) {
					return calles[i][j];
				}
			}
		}
		return null;
	}

	private boolean estaDentroDeLaCalle(Calle c) {
		return x >= c.getX() && x <= c.getX() + c.getAncho() && y >= c.getY() && y <= c.getY() + c.getAlto();
	}

	public boolean estasEnUnCruce(Calle[][] calles, Calle estoyEnEstaCalle) {
		Calle callePrevFila = calles[estoyEnEstaCalle.getCalleVertical() - 1][estoyEnEstaCalle.getCalleHorizontal()];
		Calle calleNextFila = calles[estoyEnEstaCalle.getCalleVertical() + 1][estoyEnEstaCalle.getCalleHorizontal()];
		Calle callePrevColumna = calles[estoyEnEstaCalle.getCalleVertical()][estoyEnEstaCalle.getCalleHorizontal() - 1];
		Calle calleNextColumna = calles[estoyEnEstaCalle.getCalleVertical()][estoyEnEstaCalle.getCalleHorizontal() + 1];

		return callePrevFila != null && calleNextFila != null && callePrevColumna != null && calleNextColumna != null;
	}

	public boolean sePuedeMoverHorizontal(Calle[][] calles, Calle estoyEnEstaCalle) {
		if (estoyEnEstaCalle.getCalleVertical() - 1 < 0
				|| estoyEnEstaCalle.getCalleVertical() + 1 > calles.length - 1) {
			return false; // Evita que chequee por fuera del índice del array
		}
		Calle callePrev = calles[estoyEnEstaCalle.getCalleVertical() - 1][estoyEnEstaCalle.getCalleHorizontal()];
		Calle calleNext = calles[estoyEnEstaCalle.getCalleVertical() + 1][estoyEnEstaCalle.getCalleHorizontal()];

		return (callePrev == null && calleNext == null)
				|| (estasEnUnCruce(calles, estoyEnEstaCalle) && !estasOcupandoCalleVertical(callePrev, calleNext));
	}

	public boolean sePuedeMoverVertical(Calle[][] calles, Calle estoyEnEstaCalle) {
		if (estoyEnEstaCalle.getCalleHorizontal() - 1 < 0
				|| estoyEnEstaCalle.getCalleHorizontal() + 1 > calles.length - 1) {
			return false; // Evita que chequee por fuera del índice del array
		}
		Calle callePrev = calles[estoyEnEstaCalle.getCalleVertical()][estoyEnEstaCalle.getCalleHorizontal() - 1];
		Calle calleNext = calles[estoyEnEstaCalle.getCalleVertical()][estoyEnEstaCalle.getCalleHorizontal() + 1];

		return (callePrev == null && calleNext == null)
				|| (estasEnUnCruce(calles, estoyEnEstaCalle) && !estasOcupandoCalleHorizontal(callePrev, calleNext));
	}

	public boolean estasOcupandoCalleVertical(Calle cPrev, Calle cNext) {
		return y - cPrev.getAlto() / 20 < cPrev.getY() + cPrev.getAlto() || y + cNext.getAlto() / 2 > cNext.getY();
	}

	public boolean estasOcupandoCalleHorizontal(Calle cPrev, Calle cNext) {
		return x - cPrev.getAncho() / 3 < cPrev.getX() + cPrev.getAncho() || x + cNext.getAncho() / 3 > cNext.getX();
	}

	// ####### ---------------------------------- ####### //

	public boolean estasEnLaZonaDeEntrega(Rectangle rangoHorizontal, Rectangle rangoVertical) {
		return x > rangoHorizontal.x - rangoHorizontal.width / 2 && x < rangoHorizontal.x + rangoHorizontal.width / 2
				&& y > rangoHorizontal.y - rangoHorizontal.height / 2
				&& y < rangoHorizontal.y + rangoHorizontal.height / 2
				|| x > rangoVertical.x - rangoVertical.width / 2 && x < rangoVertical.x + rangoVertical.width / 2
						&& y > rangoVertical.y - rangoVertical.height / 2
						&& y < rangoVertical.y + rangoVertical.height / 2;
	}

	public void recibeElRamo(Entorno e) {
		ramo = new Ramo();
	}

	public void dejaElRamo() {
		ramo = null;
	}

	public void movimientoOFF() {
		modoDeMovimiento = false;
	}

	public boolean chocaConNinja(Ninja ninja) {
		return (!ninja.tieneQueDesaparecer() && !ninja.tieneQueAparecer() && ninja.getX() + 30 > x
				&& ninja.getX() - 30 < x && ninja.getY() + 30 > y && ninja.getY() - 30 < y);
	}

	public boolean chocaConKunai(Kunai kunai) {
		return (kunai.getX() + 30 > x && kunai.getX() - 30 < x && kunai.getY() + 30 > y && kunai.getY() - 30 < y);
	}

	public boolean chocaConJutsu(Jutsu jutsu) {
		return (jutsu.getX() + 30 > x && jutsu.getX() - 30 < x && jutsu.getY() + 30 > y && jutsu.getY() - 30 < y);
	}

	// ####### getters ####### //

	public int aDondeEstaMirando() {
		return aDondeEstaMirando;
	}

	public boolean tieneElRamo() {
		return ramo != null;
	}

}